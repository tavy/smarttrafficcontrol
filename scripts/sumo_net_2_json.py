import json
import sys
import math
import xml.etree.ElementTree as ET

points = []

minXY = None
maxXY = None

def scalePos(pos, scale):
    pos[0] = pos[0]/scale[0]
    pos[1] = pos[1]/scale[1]
    return pos

def rotatePos(pos, angle):
    pos[0] = pos[0] * math.cos(-angle) - pos[1] * math.sin(-angle)
    pos[1] = pos[1] * math.cos(-angle) - pos[0] * math.sin(-angle)
    return pos

def translatePos(pos, how):
    pos[0] = pos[0]+how[0]
    pos[1] = pos[1]+how[1]
    return pos

def processPoint(xy):
    global points
    points.append(xy);

def processShape(shape):
    res = []
    c = shape.split(" ")
    for point in c:
        xy = [float(x) for x in point.split(",")]
        # Rotating
        xy = rotatePos(xy, math.pi)
        xy = scalePos(xy, (ratio, ratio))
        # Apply scale for mirroring
        xy = scalePos(xy, (-1,1))
        #xy = translatePos(xy, (300, 1600))
        processPoint(xy)
        res.append(xy)
    return res

def filterLane(node):
    att = node.attrib
    if "shape" in att:
        att["shape"] = processShape(att["shape"])
    return att

def filterEdge(node):
    notvalidtypes = ["railway.rail"]
    att = node.attrib
    res = att.copy()
    res["lane"] = []
    if "function" in att and att["function"] == "internal":
        return False
    if "type" in att and att["type"] in notvalidtypes:
        return False
    for lane in node.findall("lane"):
        res["lane"].append(filterLane(lane))
    if "shape" in res:
        res["shape"] = processShape(res["shape"])
    return res

def filterJunction(node):
    att = node.attrib
    if "shape" in att:
        att["shape"] = processShape(att["shape"])
    return att

def filterConnection(node):
    return None

if len(sys.argv)!= 3:
    print "Usage: python sumo_net_2_json.py ratio file_name"
    sys.exit(-1)

ratio = 1/float(sys.argv[1])
fileName = sys.argv[2]

doc = ET.parse(fileName)
root = doc.getroot()

data = {}

filters = {
   "edge": filterEdge,
   "junction": filterJunction,
   "connection": filterConnection
}

for child in root:
    tag = child.tag
    if not tag in data:
        data[tag] = []
    if tag in filters:
        fdata = filters[tag](child)
        if fdata:
            data[tag].append(fdata)
    else:    
        data[tag].append(child.attrib)



allX = [xy[0] for xy in points]
allY = [xy[1] for xy in points]
minXY = (abs(min(allX)), abs(min(allY)))
maxXY = (max(allX), max(allY))

# Normalization of points
for xy in points:
    xy[0] = round(xy[0]+minXY[0], 2)
    xy[1] = round(xy[1]+minXY[1], 2)

allX = [xy[0] for xy in points]
allY = [xy[1] for xy in points]

data["bbox"] = [(min(allX), min(allY)), (max(allX), max(allY))]
data["ratio"] = ratio
print json.dumps(data)
