// From http://phrogz.net/tmp/canvas_zoom_to_cursor.html
// Adds ctx.getTransform() - returns an SVGMatrix
// Adds ctx.transformedPoint(x,y) - returns an SVGPoint
function trackTransforms(ctx){
	var svg = document.createElementNS("http://www.w3.org/2000/svg",'svg');
	var xform = svg.createSVGMatrix();
	ctx.getTransform = function(){ return xform; };
	
	var savedTransforms = [];
	var save = ctx.save;
	ctx.save = function(){
		savedTransforms.push(xform.translate(0,0));
		return save.call(ctx);
	};
	var restore = ctx.restore;
	ctx.restore = function(){
		xform = savedTransforms.pop();
		return restore.call(ctx);
	};

	var scale = ctx.scale;
	ctx.scale = function(sx,sy){
		xform = xform.scaleNonUniform(sx,sy);
		return scale.call(ctx,sx,sy);
	};
	var rotate = ctx.rotate;
	ctx.rotate = function(radians){
		xform = xform.rotate(radians*180/Math.PI);
		return rotate.call(ctx,radians);
	};
	var translate = ctx.translate;
	ctx.translate = function(dx,dy){
		xform = xform.translate(dx,dy);
		return translate.call(ctx,dx,dy);
	};
	var transform = ctx.transform;
	ctx.transform = function(a,b,c,d,e,f){
		var m2 = svg.createSVGMatrix();
		m2.a=a; m2.b=b; m2.c=c; m2.d=d; m2.e=e; m2.f=f;
		xform = xform.multiply(m2);
		return transform.call(ctx,a,b,c,d,e,f);
	};
	var setTransform = ctx.setTransform;
	ctx.setTransform = function(a,b,c,d,e,f){
		xform.a = a;
		xform.b = b;
		xform.c = c;
		xform.d = d;
		xform.e = e;
		xform.f = f;
		return setTransform.call(ctx,a,b,c,d,e,f);
	};
	var pt  = svg.createSVGPoint();
	ctx.transformedPoint = function(x,y){
		pt.x=x; pt.y=y;
		return pt.matrixTransform(xform.inverse());
	}
}

function bboxOverlap(b1, b2) {
    return !( b1.x           > (b2.x + b2.w) || 
             (b1.x + b1.w) <  b2.x           || 
              b1.y           > (b2.y + b2.h) ||
             (b1.y + b1.h) <  b2.y);
}

function logB(base, n) {
    var tmp = Math.log(base);
    return (Math.log(n)/tmp);
}

function getRandomInt (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}

function getPageSize() {
	return {w: 	$(window).width(),
			h: $(window).height()}
}

function getRandomRgb() {
	return {r: getRandomInt(0, 255), g:getRandomInt(0, 255), b: getRandomInt(0, 255)};
}

function loadScript(url, callback) {
    // Adding the script tag to the head as suggested before
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;
	
	if(callback) {
		// Then bind the event to the callback function.
		// There are several events for cross browser compatibility.
		script.onreadystatechange = callback;
		script.onload = callback;
	}
    // Fire the loading
    head.appendChild(script);
}

function applyLayoutConfig(config, carsNIndex) {
	var carsNumber = config.cars.number || getRandomInt(config.cars.rangeNumber.min, config.cars.rangeNumber.max),
		carsToAdd, carsNIndex = carsNIndex || 0;
    
    if($.isArray(carsNumber)) {
        carsNumber = carsNumber[carsNIndex];
    }
    
    if (carsNumber > map.cars.length) {
        carsNumber-=map.cars.length;
    } else {
        map.removeAllCars();
    }
    
    carsToAdd = carsNumber;
    
	if(config.cars.dRoutes) {
		$.each(config.cars.dRoutes, function(index, obj) {
			if(carsToAdd>0) {
				var routeCars = (obj.carsPercent) ? Math.floor(carsNumber*obj.carsPercent/100) : getRandomInt(0, carsToAdd);
				carsToAdd-=routeCars;
				for(var i=0; i<routeCars; i++) {
					addCar(obj.route);
				}
			}
		});
	} else {
		for(var i=0; i<carsNumber; i++) {
			addCar();
		}
	}
}

function arrayMax(array) {
	var max_index = -1;
	var max_value = Number.MIN_VALUE;
	for(var i = 0; i < array.length; i++)
	{
		if(array[i] > max_value)
		{
			max_value = array[i];
			max_index = i;
		}
	}
	return {id: max_index, value: max_value};
}

function arrayMin(array) {
	var min_index = -1;
	var min_value = Number.MAX_VALUE;
	for(var i = 0; i < array.length; i++)
	{
		if(array[i] < min_value)
		{
			min_value = array[i];
			min_index = i;
		}
	}
	return {id: min_index, value: min_value};
}

function arrayAverage(array) {
    var sum = array.reduce(function(a, b) { return a + b });
    return sum / array.length;
}

function secondsToTime(seconds) {
    var minutes = Math.floor(seconds/60),
        hours = Math.floor(minutes/60),
        minutes = minutes % 60,
        seconds = seconds % 60;
        
    return {h: hours, m: minutes, s: seconds};
}

// Add stable merge sort to Array and jQuery prototypes
// Note: We wrap it in a closure so it doesn't pollute the global
//       namespace, but we don't put it in $(document).ready, since it's
//       not dependent on the DOM
(function() {

  // expose to Array and jQuery
  Array.prototype.mergeSort = jQuery.fn.mergeSort = mergeSort;

  function mergeSort(compare) {

    var length = this.length,
        middle = Math.floor(length / 2);

    if (!compare) {
      compare = function(left, right) {
        if (left < right)
          return -1;
        if (left == right)
          return 0;
        else
          return 1;
      };
    }

    if (length < 2)
      return this;

    return merge(
      this.slice(0, middle).mergeSort(compare),
      this.slice(middle, length).mergeSort(compare),
      compare
    );
  }

  function merge(left, right, compare) {

    var result = [];

    while (left.length > 0 || right.length > 0) {
      if (left.length > 0 && right.length > 0) {
        if (compare(left[0], right[0]) <= 0) {
          result.push(left[0]);
          left = left.slice(1);
        }
        else {
          result.push(right[0]);
          right = right.slice(1);
        }
      }
      else if (left.length > 0) {
        result.push(left[0]);
        left = left.slice(1);
      }
      else if (right.length > 0) {
        result.push(right[0]);
        right = right.slice(1);
      }
    }
    return result;
  }
})();

jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + 
                                                $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + 
                                                $(window).scrollLeft()) + "px");
    return this;
}


