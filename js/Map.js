function Map(path, staticCtx, dynamicCtx, afterInit, stepsLimit, noDraw) {
	this.ctx1 = staticCtx;
	this.ctx2 = dynamicCtx;
	this.sCtx = staticCtx;
	this.dCtx = dynamicCtx;
	this.routes = {};
	this.returnStreets = {};
	this.edges = [];
	this.junctions = [];
	this.cars = [];
	this.finishedCars = [];
    this.textVisible = false;
    this.loaded = false;
	this.zoomScaleFactor = 1.1;
    this.ratio = 1;
    this.edgeWidth = 3;
    this.carSpeed = 30;
    this.carStepXMove = 2;
    this.stepsLimit = stepsLimit;
    this.trafficLights = [];
    this.noDraw = noDraw || false;
	
	this.initialize = function(data) {
		var me = this, i, ctx1 = me.ctx1, ctx2 = me.ctx2, 
            canvas = ctx1.canvas, factor = 0;
            
        this.mapData = data;
        me.bbox = data.bbox;
        me.ratio = data.ratio;
        me.secXPixel = 1/(me.carSpeed/3.6)*me.ratio;
        me.edgeWidth = me.edgeWidth/me.ratio;
        me.trafficLights = [];
		//lastX = canvas.width/2; lastY = canvas.height/2;
        lastX = canvas.width/2; lastY = 0;
		dragStart = null;
		trackTransforms(ctx1);
		trackTransforms(ctx2);
		
		// Zoom on mouse wheel event
		$("canvas").bind('mousewheel DOMMouseScroll', function(event) {
			var evt = event.originalEvent,
				delta = evt.wheelDelta ? evt.wheelDelta/40 : evt.detail ? -evt.detail : 0;
			if (delta) me.zoom(delta);
		}).bind('mousedown', function(event) {
			var evt = event.originalEvent;
			lastX = evt.offsetX || (evt.pageX - canvas.offsetLeft);
			lastY = evt.offsetY || (evt.pageY - canvas.offsetTop);
			dragStart = ctx1.transformedPoint(lastX,lastY);
			dragged = false;
		}).bind('mouseup', function(event) {
			var evt = event.originalEvent;
			dragStart = null;
		}).bind('mousemove', function(event) {
			var evt = event.originalEvent;
			lastX = evt.offsetX || (evt.pageX - canvas.offsetLeft);
			lastY = evt.offsetY || (evt.pageY - canvas.offsetTop);
			dragged = true;
			if (dragStart){
				var pt = ctx1.transformedPoint(lastX,lastY);
				ctx1.translate(pt.x-dragStart.x,pt.y-dragStart.y);
				ctx2.translate(pt.x-dragStart.x,pt.y-dragStart.y);
				me.draw();
			}
		});
		
		if (data.edge) {
			this.edges = data.edge;
            this.initializeAllLanes();
		}
		if (data.returnStreets){
			this.returnStreets = data.returnStreets;
		}
		if (data.junction) {
			this.junctions = data.junction;
		}
        if (data.routes) {
			$.each(data.routes, function(index, obj) {
                me.routes[obj.id] = obj;
                $("#route").append('<option value="'+obj.id+'">'+obj.id+'</option>');
            });
            var onRouteOver = function() {
                    var routeId = $(this).val(), path;
                    me.draw();
                    if (routeId && me.routes[routeId]) {
                        path = me.initializeRoute(me.routes[routeId]);
                        me.drawPath(path,  {
                            lineWidth: 2,
                            strokeStyle: "rgb(200,0,0)"
                        });
                    }
            };
            $("option").hover(onRouteOver);
            $("select").change(onRouteOver);
		}
		if (data.trafficLights) {
			$.each(data.trafficLights, function(index, obj) {
				me.trafficLights.push(new Semaphore("s"+index, obj, me, me.ctx1));
            });
            $.each(me.trafficLights, function(index, sem) {
				sem.createSemNetwork();
            });
		}
        
        // Impostiamo lo zoom in modo che si veda tutta la mappa
        if(me.bbox[1][0] > me.bbox[1][1]) {
            factor = me.bbox[1][0]/canvas.width;
        } else {
            factor = me.bbox[1][1]/canvas.height;
        }
        this.zoom(logB(this.zoomScaleFactor, factor)*((factor > 1) ? -1 : 1));
        
		this.loaded = true;
		this.draw();
        
        this.initDesignerMode();
        
        $("#attribution").html(data.attribution || "");
        
        if($.isFunction(afterInit)) afterInit();
	}
	
	this.draw = function() {
        if (!this.loaded) return;
        if(!map.noDraw) {
            var p1 = this.ctx1.transformedPoint(0,0),
                p2 = this.ctx1.transformedPoint(this.ctx1.canvas.width,this.ctx1.canvas.height);
            this.ctx1.clearRect(p1.x,p1.y,p2.x-p1.x,p2.y-p1.y);
            this.ctx2.clearRect(p1.x,p1.y,p2.x-p1.x,p2.y-p1.y);
            
            for(var i = 0, lEdges = this.edges.length; i < lEdges; i++) {
                this.drawEdge(this.edges[i], this.textVisible);
            }
            
            for(var i = 0, lJunctions = this.junctions.length; i < lJunctions; i++) {
                this.drawJunction(this.junctions[i]);
            }

            for(var i = 0, lTLights = this.trafficLights.length; i < lTLights; i++) {
                this.trafficLights[i].draw();
            }
            
            this.drawPaths();
        }
		for(var i = 0, lCars = this.cars.length; i < lCars; i++) {
			if(this.cars[i]) this.cars[i].draw();
		}
        
	}
    
    this.initDesignerMode = function() {
        var me = this;
        me.dPaths = [];
        if (getURLParameter("designer") == "yes") {
            $("#menu fieldset").hide();
            $("#menu").append('<fieldset><legend>Path</legend><div class="table"><div class="column"><button id="printPath">Show</button></div><div class="column"><button id="clearPath">Clear</button></div></div></fieldset>');
            $("#printPath").click(function() {
                var path = JSON.stringify(me.dPaths.map(function(el) { return el.id}));
                alert(path);
            });
            $("#clearPath").click(function() {
                me.dPaths = [];
                me.draw();
            });
            this.designerMode = true;
            console.log("designerMode");
            var lanes = this.initializeAllLanes();
            var ctx = this.ctx1;
            $("canvas").bind('mousedown', function(event) {
                var evt = event.originalEvent;
                var rect = $("canvas")[0].getBoundingClientRect();
                var pos = {x: evt.clientX - rect.left, y:evt.clientY - rect.top};
                var pt = ctx.transformedPoint(pos.x,pos.y);
                var ls = lanes.filter(function(l) {
                    var path = l.path.filter(function(p) {
                        var diff = Math.abs(pt.x - p.x) + Math.abs(pt.y - p.y);
                        return (diff < 2);
                    });
                    return (path.length != 0);
                });
                if (ls.length != 0) {
                    me.dPaths.push(ls[0]);
                    me.drawPaths();
                }
            });
        }
    }
    
    this.drawPaths = function() {
        var me = this;
        if (!me.designerMode) return;
        $.each(me.dPaths, function(index, el) {
            me.drawPath(el.path,  {
                lineWidth: 2,
                strokeStyle: "rgb(200,0,0)"
            });
        });
    }
	
	this.drawJunction = function(config, text) {
		var ctx = this.sCtx;
		if (config.shape) {
            if (text) {
                ctx.fillStyle = "rgb(200,0,0)";
                ctx.fillText(config.id, config.shape[0][0], config.shape[0][1]);
            }
			this.drawShape(config.shape, {fill: true});
		}
	}

	this.drawEdge = function(config, text) {
		var i, ctx = this.sCtx, drawConfig = {
			lineWidth: this.edgeWidth,
			strokeStyle: (config["function"]) ? "rgb(200,0,0)" :"rgba(200,200,200, 1)"
		};
		
		for (i = 0; i < config.lane.length; i++) {
			if (config.lane[i].shape) {
                /*if(config.lane[i].possibileNextLanes.length) {
                    drawConfig.strokeStyle = "rgb(200,0,0)";
                }*/
				this.drawShape(config.lane[i].shape, drawConfig);
                if (text) {
                    ctx.fillStyle = "rgb(200,0,0)";
                    ctx.fillText(config.lane[i].id, config.lane[i].shape[0][0], config.lane[i].shape[0][1]);
                }
			}
		}
        
		/*if (config.shape) {
			drawShape(config.shape, {lineWidth: 1,strokeStyle: "rgb(200,0,0)", fillStyle: "rgb(200,0,0)"});
		}*/
	}

	this.drawShape = function(shape, config) {
		var i, ctx = this.sCtx, slength = shape.length;
		config = config || {};
		ctx.beginPath();
		ctx.strokeStyle = config.strokeStyle || "rgb(200,200,200)";
		ctx.fillStyle = config.fillStyle || "rgb(200,200,200)";
		ctx.lineWidth = config.lineWidth || 1;
		ctx.moveTo(shape[0][0], shape[0][1]);
		for (i = 1; i < slength; i++) {
			ctx.lineTo(shape[i][0], shape[i][1]);
		}
		if(config.fill) {
			ctx.fill();
		}
		ctx.stroke();
	}
    
    this.drawPath= function(shape, config) {
		var i, ctx = this.sCtx, slength = shape.length;
		config = config || {};
		ctx.beginPath();
		ctx.strokeStyle = config.strokeStyle || "rgb(200,200,200)";
		ctx.fillStyle = config.fillStyle || "rgb(200,200,200)";
		ctx.lineWidth = config.lineWidth || 1;
		ctx.moveTo(shape[0].x, shape[0].y);
		for (i = 1; i < slength; i++) {
			ctx.lineTo(shape[i].x, shape[i].y);
		}
		if(config.fill) {
			ctx.fill();
		}
		ctx.stroke();
	}
	
	this.zoom = function(delta) {
		var ctx1 = this.ctx1, ctx2 = this.ctx2,
			pt = ctx1.transformedPoint(lastX,lastY), 
            factor = Math.pow(this.zoomScaleFactor,delta);
		ctx1.translate(pt.x,pt.y);
		ctx2.translate(pt.x,pt.y);
		ctx1.scale(factor,factor);
		ctx2.scale(factor,factor);
		ctx1.translate(-pt.x,-pt.y);
		ctx2.translate(-pt.x,-pt.y);
		
		this.draw();
	}
	
	this.addCar = function(config) {
        var route = this.routes[config.routeId], lane;
        if(route.id == "randomRoute") {
            route = $.extend({}, route);
            lane = this.getRandomLane();
            route.lanes = [lane];
            this.addSemToRoute(route, lane);
            //route.lanes = [this.getLaneById("3/1to4/1_0")];
        }
		var div, car = new Car($.extend({
			plate: "c-"+getRandomInt(1, 1000)+"-"+getRandomInt(1000, 5000),
			speed: this.carStepXMove,
			realSpeedKH: this.carSpeed,
			route: route,
            ctx: this.dCtx,
            map: this
		}, config));
		if(!config.relaunch) {
			this.initializeRoute(this.routes[config.routeId]);
			div = '<div class="list-bullet" style="background-color: '+config.color+';"></div>';
			$("#carsList").append("<li>"+div+car.plate+", "+config.routeId+"</li>");
		}
		this.cars.push(car);
		$("#carCounter").html((this.cars.length+this.finishedCars.length));
	}
    
    this.removeAllCars = function() {
        this.cars = [];
        $("#carsList").html("");
    }
    
    this.computeRoutePath = function(config) {
        var i, routeNodes  = config.route, coordinates = [],
			totLength = 0, lanes = [], tmpLane, lastCoord;
        
        if(config.id == "randomRoute") {
            config.length = -1;
        } else {
            for(i = 0; i < routeNodes.length; i++) {
                var lane = this.getLaneById(routeNodes[i]),
                    ts;
                if(lanes.length) {
                    lastCoord = lanes[lanes.length-1].coordinates;
                    tmpLane = {shape: [[parseFloat(lastCoord[lastCoord.length-1].x), parseFloat(lastCoord[lastCoord.length-1].y)], lane.shape[0]]};
                    this.computeRouteLane(tmpLane, coordinates, true);
                    lanes.push(tmpLane);
                }
                if (lane) {
                    totLength+= parseFloat(lane.length);
                    this.computeRouteLane(lane, coordinates, true);
                    lanes.push(lane);
                    ts = this.searchTsByLane(lane);
                    if(ts) {
                        config.sem = config.sem || [];
                        config.sem.push({lane:lane, sem:ts});
                    }
                }
            }
            config.length = totLength;
        }
        
        return lanes;
    }
    
    this.searchTsByLane = function(lane) {
		for(var i = 0, lTLights = this.trafficLights.length; i < lTLights; i++) {
			if(this.trafficLights[i].existLane(lane)) {
				return this.trafficLights[i];
			}
		}
	}
    
    this.getRandomLane = function() {
        var randomEdge = this.edges[getRandomInt(0, this.edges.length-1)],
            randomLane = randomEdge.lane[getRandomInt(0, randomEdge.lane.length-1)];
        return randomLane;
    }
    
    this.routeAddLane = function(route, lane) {
		if (this.returnStreets[lane.id]){
            var nextLane = this.getLaneById(this.returnStreets[lane.id]);
			route.lanes.push(nextLane);
            this.addSemToRoute(route, nextLane);
			return true;
		} else {
			var nextLanes = this.getNextRandomLane(lane);
			if(nextLanes) {
				route.lanes.push(nextLanes.juncLane);
				route.lanes.push(nextLanes.lane);
				this.addSemToRoute(route, nextLanes.lane);
				return true;
			}
		}
		return false;
    }
    
    this.addSemToRoute = function(route, lane) {
        var ts = this.searchTsByLane(lane);
        if(ts) {
            route.sem = route.sem || [];
            if(!route.sem.filter(function(obj) {
                return (obj.lane == lane && obj.sem == ts);
            })[0]) {
                route.sem.push({lane:lane, sem:ts});
            }
        }
    }
    
    this.drawLane = function(lane) {
        var me = this;
        drawConfig = {
			lineWidth: this.edgeWidth,
			strokeStyle:  "rgb(200,0,0)"
		};
		
        this.drawShape(lane.shape, drawConfig);
        $.each(lane.possibileNextLanes, function(index, nextLane) {
            drawConfig.strokeStyle = "rgb(0,200,0)";
            me.drawShape(nextLane.shape, drawConfig);
        });
    }
    
    this.getNextRandomLane = function(lane) {
        if(lane.possibileNextLanes && lane.possibileNextLanes.length) {
            var index = getRandomInt(0, lane.possibileNextLanes.length-1);
            return {
                lane: lane.possibileNextLanes[index],
                juncLane: lane.possibileNextJuncLanes[index]
            };
        }
        return null;
    }
    
    this.getLaneById = function(id) {
        for(var i = 0, lEdges = this.edges.length; i < lEdges; i++) {
			var ls = this.edges[i].lane.filter(function(l) {
                return (l.id == id);
            });
            if (ls.length != 0) {
                ls[0].edge = this.edges[i];
                return ls[0];
            }
		}
        return null;
    }
    
    this.getJunctionById = function(id) {
        return this.junctions.filter(function(junc) {
            return junc.id == id;
        })[0];
    }
    
    this.getJunctionIncludingLanes = function(lanes) {
        var junction = this.junctions.filter(function(el) {
            if(el.type == "priority") {
                for(var i = 0; i < lanes.length; i++) {
                    if(el.incLanes.indexOf(lanes[i].id) == -1) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        })[0];
        return junction;
    }
    
    this.getLanesByJunction = function(junction) {
        var me = this;
        var lanes = junction.incLanes.split(" ").map(function(laneId) {
            return me.getLaneById(laneId);
        });
        return lanes;
    }
    
    this.getPossibleNextLanes = function(lane) {
        var me = this, parallelLane = this.getParallelLane(lane);
        var junction = this.getJunctionIncludingLanes([lane]);
        var lanes = [];
        
        if(junction) {
            var lanes = this.getLanesByJunction(junction).map(function(l) {
                return me.getParallelLane(l);
            }).filter(function(l) {
                return (l != parallelLane);
            });
        }
        return lanes;
    }
    
    this.getPossibleNextJuncLanes = function(lane) {
        var me = this, lanes = [], lastCoord = lane.coordinates, tmpLane;
        $.each(lane.possibileNextLanes, function(index, nLane) {
            tmpLane = {shape: [[parseFloat(lastCoord[lastCoord.length-1].x), parseFloat(lastCoord[lastCoord.length-1].y)], nLane.shape[0]]};
            me.computeRouteLane(tmpLane, [], true);
            lanes.push(tmpLane);
        });
        return lanes;
    }
    
    this.getParallelLane = function(lane) {
        this.getLaneById(lane.id); //for setting edge
        var edge = this.edges.filter(function(edge) {
            return (edge.to == lane.edge.from && edge.from == lane.edge.to);
        })[0];
        return (edge) ? edge.lane[0] : null;
    }
    
    this.getSemaphoreByJunctionId = function(id) {
        var sema = this.trafficLights.filter(function(sem) {
            return (sem.junction.id == id);
        })[0];
        return sema;
    }
    
    this.getSemaphoreByLane = function(lane) {
        var sema = this.trafficLights.filter(function(sem) {
            return (sem.lanes.indexOf(lane) != -1);
        })[0];
        if(sema) {
            var group = sema.groups.filter(function(group) {
                return (group.lanes.indexOf(lane) != -1);
            })[0];
            return {
                sem: sema,
                group: sema.groups.indexOf(group),
                lane: lane
            };
        }
    }
    
    this.computeRouteLane = function(lane, coordinates, noNext) {
        var totAmount, startX, startY, endX, endY, amount = 0, i,
			coordinates = coordinates || [], localCoordinates = [];

            startX = lane.shape[0][0];
            startY = lane.shape[0][1];
            i = 1;
        for(;i < lane.shape.length; i++) {
            endX = lane.shape[i][0];
            endY = lane.shape[i][1];
            deltaX = endX - startX;
            deltaY = endY - startY;
            distance =  Number(Math.sqrt(deltaX*deltaX + deltaY*deltaY)).toFixed(2);
            amount = 1/distance;
            totAmount = 0;
            for(var j = 0; j < distance; j++) {
                totAmount+=amount;
                //Interpolazione lineare
                newX = Number(startX + ((endX - startX) * totAmount)).toFixed(2);
                newY = Number(startY + ((endY - startY) * totAmount)).toFixed(2);
                coordinates.push({x:newX, y:newY});
                localCoordinates.push({x:newX, y:newY});
            }
            startX = endX;
            startY = endY;
        }
        lane.coordinates = localCoordinates;
        if(!noNext) {
            lane.possibileNextLanes = this.getPossibleNextLanes(lane);
            lane.possibileNextJuncLanes = this.getPossibleNextJuncLanes(lane);
        }
        
        return coordinates;
    },
    
    this.initializeRoute = function(route) {
        //drawRoute(config.route, getContext(), config.color, 3, 0.3);
        route.lanes = (!route.lanes) ? this.computeRoutePath(route) : route.lanes;
        return route.lanes;
    }
    
    this.initializeAllLanes = function() {
        var coordinates = [];
        for(var i = 0, lEdges = this.edges.length; i < lEdges; i++) {
			for(var j = 0, lLanes = this.edges[i].lane.length; j < lLanes; j++) {
                var laneCoord = [];
                coordinates.push({id:this.edges[i].lane[j].id, path: this.computeRouteLane(this.edges[i].lane[j], laneCoord)});
            }
		}
        return coordinates;
    }

    this.moveCars = function(start) {
        //console.time("moveC");
        this.firstTime = this.firstTime || window.performance.now();
		//console.log("MOVEEEE CARS!!!!!___________________________________________________________________________");
		var newFn = $.proxy(this.semLogic.checkAndChange, this.semLogic),
			finishedCars = 0, car, lCars = this.cars.length, i, activeCars = 0;
			
		for(var i = 0, lTLights = this.trafficLights.length; i < lTLights; i++) {
			newFn(this.trafficLights[i]);
		}
		//this.dCtx.clearRect(0, 0, this.dCtx.canvas.width, this.dCtx.canvas.height);
		if(lCars) {
			for(i = 0; i < lCars; i++) {
				car = this.cars[i];
				if(car) {
					//console.log("____moving cars: ", car.plate, JSON.stringify(car.pos));
					if (start || (!car.started && !car.finished)) {
						car.start();
					} else if (car.finished) {
						this.finishedCars[i] = car;
						this.cars[i] = null;
					}
					if(!start) {
						car.move();
					}
					activeCars++;
				}
			}
		}
		if(!activeCars) {
            console.log(this.actualSimulation.counterSteps);
			this.cars = [];
            if(generation) {
                generation.endSimulation();
            }
            if(simulation) {
                simulation.end();
            }
            return false;
		} else if(this.stepsLimit && this.actualSimulation.counterSteps >= this.stepsLimit) {
            console.log(this.actualSimulation.counterSteps);
            this.stopSimulation();
            return false;
        }
        this.actualSimulation.counterSteps++;
        //console.timeEnd("moveC");
        return true;
    }
    
    
    this.stopSimulation = function() {
        var lCars, car;
        for(i = 0, lCars=this.cars.length; i < lCars; i++) {
            car = this.cars[i];
            if(car) {
                car.finish(true);
                this.finishedCars[i] = car;
                this.cars[i] = null;
            }
        }
        this.cars = [];
        if(generation) {
            generation.endSimulation();
        }
        if(simulation) {
            simulation.end();
        }
    }
    
    this.canMove = function(car, bbox, posIndex) {
        //var securitySteps = 3,
		var securitySteps = 10,
			securityPos = car.getNextPosIndex(securitySteps, posIndex);
			pathStep = car.getCoordinates(securityPos);
		
		while(pathStep && securitySteps) {
			securityPos = car.getNextPosIndex(--securitySteps, posIndex);
			pathStep = car.getCoordinates(securityPos);
			//console.log(car.plate, securitySteps, JSON.stringify(securityPos), JSON.stringify(pathStep));
			if(securityPos) {
				if(pathStep && pathStep.taken === true) {
					//console.log(car.plate+" return false 1");
					return false;
				}
			}
		}
		
        if(!this.canPassSem(car, bbox)) return false;
        return true;
    }
	
	this.canPassSem = function(car, bbox) {
		var sem = car.route.sem, lane, semObj;
		if(sem) {
			for(var i = 0, lSem = sem.length; i < lSem; i++) {
				semObj = sem[i].sem;
				lane = semObj.getLaneByBbox(bbox);
				if (lane && !semObj.canPassLane(lane)) {
					return false;
				}
			}
		}
        return true;
	}
    
    this.mixSem = function(semObj) {
        var newFn = $.proxy(this.semLogic.checkAndChange, this.semLogic);
        if(Math.random() < 0.9) {
            var times = getRandomInt(1, 20);
            for(var i = 0; i < times; i++) {
                newFn(semObj);
            }
        }
    }
	
	this.start = function(delay) {
		this.delay = delay,plates = [];
        
		for(var i = 0, lTLights = this.trafficLights.length; i < lTLights; i++) {
			this.trafficLights[i].start();
            this.mixSem(this.trafficLights[i]);
		}
        for(i = 0; i < this.cars.length; i++) {
            plates.push(this.cars[i].plate);
        }
        this.finishedCars = new Array(this.cars.length);
	}
	$.getJSON(path, $.proxy(this.initialize, this));
}
