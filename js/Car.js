function Car(config) {
	this.color = config.color;
	this.plate = config.plate;
	this.speed = config.speed;
	this.route = config.route;
	this.realSpeedKH = config.realSpeedKH;
    this.ctx = config.ctx;
    this.map = config.map;
	this.posIndex = {
		lane: 0,
		pos: 0
	};
	this.oldPosIndex = {};
    this.score = 0;
    this.rad = 1.5;
    this.size = {w: this.rad*2, h: this.rad*2};
    this.pos = {x: -1, y: -1};
    this.fPos = {x: -1, y: -1};
    this.bPos = {x: -1, y: -1};
    this.border = 0.3;
    this.steps = 0;
    this.started = false;
	this.finished = false;
	this.secPerPixel = 1/(this.realSpeedKH/3.6)*this.map.ratio;
	this.draw = function() {
		var ctx = this.ctx;
        if (this.started) {
            this.updatePos();
        }
        if(this.map.noDraw) return;
        //this.drawRaw();
        ctx.beginPath();
        
        ctx.arc(this.pos.x, this.pos.y, this.rad,0,Math.PI*2,true);
        ctx.lineWidth = this.border;
        ctx.strokeStyle = "rgb(0,0,0)";
		ctx.fillStyle = this.color;
        ctx.fill();
        ctx.stroke();
	}
    
    
    this.drawRaw = function() {
        var ctx = this.ctx;
        ctx.beginPath();
        ctx.moveTo(Math.round(this.bPos.x), Math.round(this.bPos.y));
        ctx.lineTo(Math.round(this.fPos.x), Math.round(this.fPos.y));
		ctx.lineWidth = Math.round(this.size.h);
        ctx.strokeStyle = this.color;
		//ctx.fillStyle = this.color;
		//ctx.fill();
        ctx.stroke();
    }
	
	this.move = function() {
        var speed = this.speed,
			newPos = this.getNextPosIndex(speed);
		
		if(!newPos) {
			this.started = false;
			this.finished = true;
		}
		
		if (this.finished) {
			this.remove(); 
			return;
		}
		
		if (this.started) {
			this.score += speed;
		}
        if (!this.started || !this.map.canMove(this, this.getBbox(this.getPos(newPos)), newPos)) {
			return;
		}
        this.steps += speed;
		this.clear();
		this.oldPosIndex = this.posIndex;
		this.posIndex = newPos;
		this.draw();
	}
    
    this.finish = function(penality) {
        if(penality) {
            penality = (this.started) ? this.getRouteLength() : this.getRouteLength()*2;
            if(this.route.id != "randomRoute") {
                this.score+=penality;
            }
        }
        this.started = false;
        this.finished = true;
        this.remove();
    }
    
    this.getBbox = function(newPos) {
        var pos = (newPos) ? newPos : this.pos;
        //return {x:pos.x-1, y: pos.y-1, w: this.size.w+1, h:this.size.h+1};
        return {x:pos.x-this.rad-this.border, y: pos.y-this.rad-this.border, w: this.size.w+(this.border*2), h:this.size.h+(this.border*2)};
        //return {x:this.bPos.x, y: this.bPos.y-(this.size.h/2), w: (this.fPos.x-this.bPos.x), h:this.size.h};
    }

	this.remove = function() {
		this.getCoordinates().taken = null;
		this.clear();
	}
	
	this.getAvgSpeed = function() {
		return (this.route.length/(this.score*this.secPerPixel)*3.6);
	}
	
	this.getFinalScore = function() {
		return ((this.score / this.getRouteLength())-1)*100;
	}
	
	this.getRouteLength = function () {
		var length = 0;
		$.each(this.route.lanes, function(index, el) {
			length+= el.coordinates.length;
		});
		return length;
	}
    
    this.updatePos = function() {
		var pos = this.getCoordinates(this.posIndex),
			oldPos = this.getCoordinates(this.oldPosIndex),
            posN = this.getCoordinates(this.getNextPosIndex(5));
		if(oldPos) {
			oldPos.taken = null;
			//console.log("POS NULL ", this.plate, JSON.stringify(this.oldPosIndex), JSON.stringify(oldPos));
		}
        if (posN && posN.x !== undefined && posN.y !== undefined) {
            this.fPos = posN;
            //console.log("POS TRUE ", this.plate, JSON.stringify(this.posIndex), JSON.stringify(this.pos));
        }
        if (pos && pos.x !== undefined && pos.y !== undefined) {
            this.pos = pos;
            this.bPos = pos;
            this.pos.taken = true;
            //console.log("POS TRUE ", this.plate, JSON.stringify(this.posIndex), JSON.stringify(this.pos));
        } else {
            this.started = false;
			this.finished = true;
        }
    }
    
    
    this.getNextPosIndex = function(speed, posIndex) {
		var pos, laneLength, expectedIndex;
		
        posIndex = posIndex || this.posIndex;
        posIndex = $.extend({}, posIndex),
        posIndex.pos += speed;
        expectedIndex = posIndex.pos;
        laneLength = this.route.lanes[posIndex.lane].coordinates.length;
        pos = this.getCoordinates(posIndex);
        if(!pos) {
            posIndex = $.extend({}, this.posIndex);
            if(this.route.id == "randomRoute" && posIndex.lane >= this.route.lanes.length-1) {
                if(!this.map.routeAddLane(this.route, this.route.lanes[posIndex.lane])) {
                    return null;
                }
            }
            posIndex.lane++;
            posIndex.pos = expectedIndex - laneLength;
            pos = this.getCoordinates(posIndex);
            if(pos) {
                return posIndex;
            } else {
                return null;
            }
        }
		return posIndex;
	}
	
	this.getPos = function(posIndex) {
		return this.getCoordinates(posIndex);
	}
	
	this.getCoordinates = function(posIndex) {
		posIndex = posIndex || this.posIndex;
		if(posIndex.lane !== undefined && posIndex.pos !== undefined) {
			var lane = this.route.lanes[posIndex.lane], step;
			if (lane) {
				return lane.coordinates[posIndex.pos];
			}
		}
		return null;
	}
    
    this.start = function() {
		this.posIndex = {lane: 0, pos: 0};
		this.pos = {x: -1, y: -1};
        this.steps = 0;
        if(this.map.canMove(this,this.getBbox(this.getPos(this.posIndex)), this.posIndex)) {
            this.started = true;
            this.finished = false;
			this.draw();
        }
    }
	
	this.clear = function() {
		var bbox = this.getBbox();
		this.ctx.clearRect(bbox.x, bbox.y, bbox.w, bbox.h);
        
        /*this.ctx.globalCompositeOperation="xor";
        this.drawRaw();
        this.ctx.globalCompositeOperation="source-over";*/
	}
	this.route.color = this.color;
}
