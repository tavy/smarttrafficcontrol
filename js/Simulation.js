function Simulation(inputId, noDelay, onFinish) {
    this.moveInterval = null;
    this.started = false;
    this.inputId = inputId;
    this.delay = 0;
    this.result = {cars: []};
    this.timerName = "simulation";
    this.noDelay = noDelay || false;
    this.onFinish = onFinish;
    
    this.setDelay = function(delay) {
		var delta, full;
		full = map.secXPixel*map.carStepXMove*1000;
		delay = parseInt(delay);

		delta = delay - 100;
        delta = (delta<0) ? delta*5 : delta;
        
        this.delay = full+Math.round(-1.1*delta);
        if(map.noDraw || this.noDelay) {
            this.delay = 1;
        }
        if (this.moveInterval) {
            clearInterval(this.moveInterval);
        }
        if (this.started) {
            this.moveInterval = setInterval($.proxy(map.moveCars, map), this.delay);
        }
        map.delay = this.delay;
    }
    
    this.start = function(dna, semLogic) {
        this.counterSteps = 0;
        this.semLogic = semLogic || $("#semLogic").val();
		map.semLogic = semaphores[this.semLogic];
        
        map.actualSimulation = this;
        
        if(dna) {
            semaphores.dna.setDna(dna);
            this.dna = dna;
        } else if(map.semLogic.dna) {
            this.dna = map.semLogic.dna;
        }
        
        if (!this.started) {
            setUpHideBar(simulation);
            this.started = true;
            this.setDelay($(this.inputId).val());
            $.proxy(map.start, map)(this.delay);
			//console.time(this.timerName);
			$.proxy(map.moveCars, map)(true);
        }
    }
    
    this.setUpCars = function() {
		var finishedCars;
		finishedCars = map.finishedCars;
		map.finishedCars = [];
		$.each(finishedCars, function(index, car) {
			if(car && car.finished) {
				$.proxy(map.addCar, map)({
					color: car.color,
					routeId:  car.route.id,
					plate: car.plate,
					relaunch: true
				});
			}
		});
		
    }
    
    this.pause = function() {
		if (this.moveInterval) {
            clearInterval(this.moveInterval);
        }
	}
    
    this.end = function() {
		this.started = false;
		this.saveResult();
		setUpHideBar(simulation, true);
		if (this.moveInterval) {
            clearInterval(this.moveInterval);
        }
        //console.timeEnd(this.timerName);
        this.setUpCars();
        map.actualSimulation = false;
        if($.isFunction(this.onFinish)) {
            this.onFinish(this.result);
        }
	}
	
	this.saveResult = function() {
		var totScore = 0, totSteps = 0;
		for(i = 0, lCars = map.finishedCars.length; i < lCars; i++) {
			car = map.finishedCars[i];
            if(car) {
                this.result.cars[i] = {
                    avgSpeed: car.getAvgSpeed(),
                    plate: car.plate,
                    score: car.getFinalScore(),
                    duration: (car.score*car.secPerPixel),
                    routeId: car.route.id,
                    color: car.color,
                    steps: car.steps
                }
                /*if(car.getFinalScore()<0) {
                    console.log(car.getFinalScore(), car);
                }*/
                totScore+=car.getFinalScore();
                totSteps+=car.steps;
            }
		}
		this.result.avgScore = parseFloat((totScore/lCars).toFixed(4));
        this.result.avgSteps = parseFloat((totSteps/lCars).toFixed(4));
		this.result.semaphore = this.semLogic;
        this.result.dna = map.actualSimulation.dna;
	}
    
}
