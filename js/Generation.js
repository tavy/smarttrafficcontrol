function Generation(simulationInputId, onFinish){
	this.simulationInputId = simulationInputId;
	this.simulations = [];
	this.numberOfSimulations = 50;
    this.numberOfGenerations = 10;
	this.numberOfBestDnas = 1;
	this.numberOfDnaToMutate = Math.round(this.numberOfSimulations / 10);
	this.numberOfDnaToCrossover = Math.round(this.numberOfSimulations*89 / 100);
	this.actualSim = 0;
	this.totScore = 0;
	this.results = [],
    this.dnas = [];
    this.onFinish = onFinish;
    this.simulationStartTime;
    this.simulationEndTime;
    this.simulationExecutionTimes = [];
    this.finished = false;
    this.lastBestSimulations = {};
	
	this.start = function() {
        var me = this;
        if(!this.results.length) {
            console.time("generation");
            $('#startSimulation').toggleClass("disabled");
        }
        if(this.lastBestSimulations.simulations) {
            this.simulations = this.lastBestSimulations.simulations;
            $.each(this.lastBestSimulations.simulations, function(index, sim) {
                generation.next(true);
            });
            var simulation = new Simulation(this.simulationInputId, true, function() {
                generation.next();
            });
            
            this.simulations.push(simulation);
            simulation.start(this.dnas[this.actualSim]);
        } else {
            this.simulations = [new Simulation(this.simulationInputId, true, function() {
                generation.next();
            })];
            this.simulations[this.actualSim].start(this.dnas[this.actualSim]);
        }
        
        this.simulationStartTime = performance.now();
	}

	this.next = function(noSimulation){
        this.simulationEndTime = performance.now();
		var genNumber = this.results.length + 1,
            simNumber = this.actualSim+1,
            simulationExecutionTime = this.simulationEndTime-this.simulationStartTime,
            timesInteress = [], 
            avgTime,
            totalSimulations = (this.numberOfSimulations*this.numberOfGenerations),
            finishedSimulations = simNumber+(this.results.length*this.numberOfSimulations),
            remainingSimulations = totalSimulations-finishedSimulations,
            timeLeft, timeLeftHtml = "";
            
        this.simulationExecutionTimes.push(simulationExecutionTime);
        
        timesInteress = (this.simulationExecutionTimes.length <= 10) ? this.simulationExecutionTimes : this.simulationExecutionTimes.slice(this.simulationExecutionTimes.length-10);
        
        avgTime = Math.round(arrayAverage(timesInteress)/1000);
        timeLeft = secondsToTime(remainingSimulations*avgTime);
        timeLeftHtml+= (timeLeft.h<10) ? "0"+timeLeft.h : timeLeft.h;
        timeLeftHtml+=":";
        timeLeftHtml+= (timeLeft.m<10) ? "0"+timeLeft.m : timeLeft.m;
        timeLeftHtml+=":";
        timeLeftHtml+= (timeLeft.s<10) ? "0"+timeLeft.s : timeLeft.s;
        $("#timeLeft").html(timeLeftHtml);
        /*if(noSimulation) {
            simNumber+=" .. "+noSimulation;
        }*/
		$("#simulationList").append('<li>Gen. # ' + genNumber + ' | Sim. # '+ simNumber+ ' - ' + this.simulations[this.actualSim].result.avgScore+'</li>');
        $("#simulationList").scrollTop($("#simulationList")[0].scrollHeight);
        $('#progressbarGen').val(Math.round((finishedSimulations/totalSimulations)*100));
		this.totScore += this.simulations[this.actualSim].result.avgScore;
        
		if (!this.finished && this.simulations.length < this.numberOfSimulations){
            this.simulationStartTime = performance.now();
            this.actualSim++;
            if(!noSimulation) {
                var simulation = new Simulation(this.simulationInputId, true, function() {
                    generation.next();
                });
                this.simulations.push(simulation);
                simulation.start(this.dnas[this.actualSim]);
            }
            
		}
		else this.end();
	}
    

	this.end = function(){
		var result = {simulations: []};
		result.avgScore = this.totScore / this.numberOfSimulations;
		result.simulations = this.simulations;
		this.results.push(result);
		this.actualSim = 0;
		this.totScore = 0;
        var bestSims = this.getBestDna(result.simulations);
        this.lastBestSimulations = bestSims;
        //this.dnas = this.reproduceGen(bestSims.dna);
        this.dnas = this.reproduceGenNew(this.getSimulationsData(result.simulations));
        $('#saveDna').removeClass("disabled");
        if(!this.finished && this.results.length < this.numberOfGenerations) {
            this.start();
        } else {
            console.timeEnd("generation");
            $('#startSimulation').toggleClass("disabled");
            if(result) {
                bestSims = this.getBestDna(result.simulations);
            }
            if($.isFunction(this.onFinish)) {
                this.onFinish(bestSims.dna[0]);
            }
        }
	}
    
    this.getOrderedDna = function(simulations) {
        return simulations.mergeSort(function(a,b) {
            return a.result.avgScore > b.result.avgScore;
        }).map(function(sim){
            return sim.result.dna;
        });
    }
    
    this.getSimulationsData = function(simulations) {
        return simulations.map(function(sim){
            return {
                dna: sim.result.dna,
                score: sim.result.avgScore
            };
        });
    }
    
    this.getBestDna = function(simulations) {
        var orderedSims = simulations.mergeSort(function(a,b) {
            return a.result.avgScore > b.result.avgScore;
        });
        var result = {
            simulations : orderedSims.slice(0,this.numberOfBestDnas)
        };
        result.dna = result.simulations.map(function(sim){
            return sim.result.dna;
        });
        return result;
    }
    
    this.finish = function() {
        this.finished = true;
        map.stopSimulation();
    }
    
    this.reproduceGenNew = function(simulations) {
        console.log(simulations);
        var newDna = [], i;
        
        simulations = simulations.mergeSort(function(a,b) {
            return a.score > b.score;
        });
        
        simulationsNoWorst = simulations.slice(0,simulations.length-Math.round(simulations.length*0.1));
        
        newDna.push(simulations[0].dna);
        
        for (i=1; i < this.simulations.length; i++) {
			var sim1 = this.getRandSim(simulationsNoWorst),
                sim2 = this.getRandSim(simulationsNoWorst);

            newDna.push(this.crossoverDnaNew(sim1.dna, sim2.dna));
		}

        for (i=1; i < newDna.length; i++) {
			newDna[i] = this.mutateDnaNew(newDna[i]);
		}
        
        return newDna;
    }
    
    this.crossoverDnaNew = function(dna1, dna2) {
        if(Math.random() < 0.51) {
            return this.crossoverDna(dna1, dna2);
        } else {
            return this.crossoverDnaUniform(dna1, dna2);
        }
    }
    
    this.crossoverDnaUniform = function(dna1, dna2) {
        var newDna = "", uniformRate = 0.5;
        for(var i = 0; i < dna1.length; i++) {
            if(Math.random() <= uniformRate) {
                newDna+= dna1.charAt(i);
            } else {
                newDna+= dna2.charAt(i);
            }
        }
        
        return newDna;
    }
    
    this.mutateDnaNew = function(dna) {
        var newDna = "", mutationRate = 0.015;
        for(var i = 0; i < dna.length; i++) {
            if(Math.random() < mutationRate) {
                newDna+= (dna.charAt(i) === "0") ? "1" : "0";
            } else {
                newDna+= dna.charAt(i);
            }
        }
        
        return newDna;
    }
    
    this.getRandSim = function(simulations) {
        var sims = new Array(5), rand;
        
        for(var i = 0; i < sims.length; i++) {
            rand = getRandomInt(0,simulations.length-1);
            sims[i] = simulations[rand];
        }
        
        return sims.mergeSort(function(a,b) {
            return a.score > b.score;
        })[0];
    }
    
    this.reproduceGen = function(dnas) {
        // Un 10% dei DNA deve essere uguale ai migliori della generazione precedente
        var newDna = [],
        i,j;
        
        newDna = newDna.concat(dnas);
        
        //89% cross-over
        for (i=0; i<this.numberOfDnaToCrossover; i++){
			do{
				j = getRandomInt(0,dnas.length-1);
			}while( (i % dnas.length) == j);
			newDna.push(this.crossoverDna(dnas[i % dnas.length],dnas[j]));
        }
        
        //Muto casualmente un 10% dei dna ottenuti
        for (i=0; i<this.numberOfDnaToMutate; i++){
			j = getRandomInt(1,newDna.length-1); //Tengo il migliore per sicurezza
			newDna[j] = this.mutateDna(newDna[j]);
		}
			
		
        //1% random
        console.log(newDna.length);
        newDna = newDna.concat(this.generateNDnas(this.numberOfSimulations-newDna.length));
        console.log(newDna);
        return newDna;
    }
    
    this.mutateDna = function(dna){
		var mutateIndex = getRandomInt(0,dna.length),
			charToMutate = dna.charAt(mutateIndex),
			mutateToChar = (charToMutate === "0" ? "1" : "0"),
			newDna = dna.substr(0, mutateIndex) + mutateToChar + dna.substr(mutateIndex+1);
			
		return newDna;	
		
	}
    
    this.crossoverDna = function(dna1, dna2) {
        var splitIndex = getRandomInt(0, dna1.length),
            dna1First = dna1.substring(0, splitIndex), 
            dna2Second = dna2.substring(splitIndex);
            
        return dna1First+dna2Second;
    }

	this.setDelay = function(delay){
		this.simulations[this.actualSim].setDelay(delay);
	}

	this.endSimulation = function(){
		this.simulations[this.actualSim].end();
	}

	this.simStarted = function(){
		var lastSim = this.simulations.length - 1;
                return this.simulations[lastSim].started;
	}
    
    this.generateDnas = function() {
        this.dnas = [];
        var dnaLength = Math.pow(2,semaphores.dna.getStateLength()), dna;
        for(var i = 0; i < this.numberOfSimulations; i++) {
            dna = getRandomBinaryString(dnaLength);
            this.dnas.push(dna);
            
        }
    }
    
    this.generateNDnas = function(n) {
        var dnaLength = Math.pow(2,semaphores.dna.getStateLength()), dnas= [];
        for(var i = 0; i < n; i++) {
            dnas.push(getRandomBinaryString(dnaLength));
        }
        return dnas;
    }
    
    this.generateDnas();
}
