function initializeDelaySelector(config) {
    $("#delayInput").attr(config);
    //$("#delayValue").html($("#delayInput").val());
    $("#delayInput").change(function(){ 
        var value = $(this).val();
        //$("#delayValue").html(value);
        if(generation) {
            generation.setDelay(value);
        }
        if(simulation) {
            simulation.setDelay(value);
        }
    });
}

function setUpHideBar(simulation, stop) {
    return;
	var sideBar = $("#sidebar"), 
		leftSlide = ($("#sidebar").width()-20),
		sideOut = true,
		mouseOver = function() {
			if(sideBar.position().left == 0) return;
			sideBar.animate({
				opacity: 1,
				left: "+="+leftSlide
			}, {
				start: function() {
					sideBar.off('mouseover');
				},
				complete: function() {
					if(generation.simStarted()) {
						sideBar.mouseout(mouseOut);
					}
				}
			});
		},
		mouseOut = function(event) {
			if(!$(event.relatedTarget).parents("#sidebar")[0]) {
				sideBar.animate({
					opacity: 0.30,
					left: "-="+leftSlide
				}, {
					start: function() {
						sideBar.off('mouseout');
					},
					complete: function() {
						sideBar.mouseover(mouseOver);
					}}
				);
			}
		};
	if(stop) {
		sideBar.off("mouseout");
		mouseOver();
	} else {
		sideBar.mouseout(mouseOut);
	}
}

function setElementsSize() {
	var height = $(window).height()-5,
		width = $(window).width() -5,
		attribution = $("#attribution");
	$("canvas").attr({'height':height, 'width':width});
	$(".topLayer").attr({'height':height, 'width':width});
	$("#sidebar").css({"height": height});
	
	if (map) {
		map.draw();
	}
}

function addCar(routeId) {
	var car, color = $('#colorSelector div').css('backgroundColor'),
		routeId = routeId || $("#route").val(), routesList = [];
	
	if (!routeId) {
		for (routeId in map.routes) {
			routesList.push(routeId);
		}
		routeId = routesList[getRandomInt(0, routesList.length-1)];
	}
	$.proxy(map.addCar, map)({
		color: color,
		routeId: routeId
	});
	setRandomColor();
}

function setRandomColor() {
	var rgb = getRandomRgb();
	$('#colorSelector').ColorPickerSetColor(rgb);
	$('#colorSelector div').css('backgroundColor', 'rgb(' + rgb.r+','+ rgb.g + ',' + rgb.b +')');
}
 
function initSemaphores() {
var getStrCopy = function (str, copies) {
	var newStr = str;
	copies = (copies > 0) ? copies : 1;
	while (--copies) {
		newStr += str;
	}
	return newStr;
},
convertDecToBase = function ( dec, base, length, padding ) {
	padding = padding || '0' ;
	var num = dec.toString( base );
	length = length || num.length;
	if (num.length !== length) {
		if (num.length < length) {
			num = getStrCopy( padding, (length - num.length)) + num;
		}
		else {
			throw new Error("convertDecToBase(): num(" + num + ").length > length(" + length + ") too long.");
		}
	}
	return num;
};


getRandomBinaryString = function(length) {
    //var result = "00100001010010000100011110011100000100110011001110101110001100010100001101110111111111001111110100100010110001000000101100011101";
    var result = "";
    for(var i=0; i<length;i++) {
        var value = getRandomInt(0,1);
        result += ""+value;
    }
    return result;
};

	semaphores = {
		dna : { 
            counter: 22, //il tempo per far passare 5 auto.
            maxCounter: 200,
            considerAdiacentSems: true,
            dna: null,
			checkAndChange: function(semObj) {
				var me = this;
				var pointsOfInteress = 150,
                    maxCarsInLane = pointsOfInteress*0.1;

				semObj.tmpCounter = semObj.tmpCounter || 0;
                semObj.tmpCounterMax = semObj.tmpCounterMax || 0;
				
                
                if(semObj.tmpCounterMax++ >= me.maxCounter) {
                    semObj.disableGroup((semObj.activeGroup+1) % semObj.gLength);
                    semObj.tmpCounterMax = 0;
                    semObj.tmpCounter = 0;
                }
				else if(semObj.tmpCounter++ == me.counter) {
					var act = semObj.activeGroup;
					var stringAct = convertDecToBase(act,2,1);
					var	carsInGroups = semObj.getCarsInGroups(pointsOfInteress),
						maxItem = arrayMax(carsInGroups),
                        fewCarRate = 8, // circa la media tra 5 e 10 cioe' le auto che passano in una corsia o nel gruppo in un tick.
                        adiacentBits = ["1", "1"]; //tutti i gruppi "1" via libera di default
                        
					for (var j=0; j<2; j++){
						var numberOfCars = carsInGroups[j],
                            score = Math.ceil(numberOfCars/fewCarRate);
                        
                        score = (score > 3) ? 3 : score;
                        stringAct+=convertDecToBase(score,2,2);
                        
                        if(me.considerAdiacentSems) {
                            numberOfCars = 0;
                            var adiacentSemIndex = j*semObj.groups[j].lanes.length;
                            if(semObj.adiacentSems[adiacentSemIndex] && semObj.adiacentSems[adiacentSemIndex].sem.activeGroup != semObj.adiacentSems[adiacentSemIndex].group && 
                                semObj.adiacentSems[adiacentSemIndex+1] && semObj.adiacentSems[adiacentSemIndex+1].sem.activeGroup != semObj.adiacentSems[adiacentSemIndex+1].group) {
                                adiacentBits[j] = "0";
                            }
                            
                            if(semObj.adiacentSems[adiacentSemIndex]) {
                                numberOfCars+=semObj.adiacentSems[adiacentSemIndex].sem.countCarsInLane(semObj.adiacentSems[adiacentSemIndex].lane);
                            }
                            if(semObj.adiacentSems[adiacentSemIndex+1]) {
                                numberOfCars+=semObj.adiacentSems[adiacentSemIndex+1].sem.countCarsInLane(semObj.adiacentSems[adiacentSemIndex+1].lane);
                            }
                            score = Math.ceil(numberOfCars/fewCarRate);
                            score = (score > 3) ? 3 : score;
                            try{
                                stringAct+=convertDecToBase(score,2,2);
                            }catch(e) {
                                console.log(score);
                            }
                        }
                        
					}
                    if(me.considerAdiacentSems) {
                        stringAct+=adiacentBits.join("");
                    }
                    
                    var change = parseInt(me.dna[parseInt(stringAct,2)]);
                    if(change) {
                        semObj.disableGroup((semObj.activeGroup+1) % semObj.gLength);
                        semObj.tmpCounterMax = 0;
                    }
					semObj.tmpCounter = 0;
				}
				
			},
            setDna: function(dna) {
                this.dna = dna;
                this.onSelect();
            },
            onSelect: function() {
                var dna = this.dna || "";
                $('fieldset.dna').show();
                $('#dnaData textarea').val(dna);
            },
            onDeselect: function() {
                $('fieldset.dna').hide();
            },
            isErrorState: function() {
                return (this.dna) ? false : "DNA is empty";
            },
            getStateLength: function() {
                return (this.considerAdiacentSems) ? 11 : 5;
            }
		},
		dumb : {
			counter: 50,
			checkAndChange: function(semObj) {
				var me = this;
				semObj.tmpCounter = semObj.tmpCounter || 0;
                //semObj.firstA = semObj.firstA || window.performance.now();
				if(semObj.tmpCounter++ == me.counter) {
					semObj.disableGroup((semObj.activeGroup+1) % semObj.gLength);
					semObj.tmpCounter = 0;
                    //console.log(window.performance.now()-semObj.firstA);
				}
			}
		},
		smart : {
			counter: 22,
			maxCounter: 200,
			checkAndChange: function(semObj) {
				var me = this, pointsOfInteress = 50;
				semObj.tmpCounter = semObj.tmpCounter || 0;
				semObj.tmpCounterMax = semObj.tmpCounterMax || 0;

				
				 if(semObj.tmpCounterMax++ >= me.maxCounter) {
                    semObj.disableGroup((semObj.activeGroup+1) % semObj.gLength);
                    semObj.tmpCounterMax = 0;
                    semObj.tmpCounter = 0;
                }
				else if(semObj.tmpCounter++ == me.counter) {
					var	carsInGroups = semObj.getCarsInGroups(pointsOfInteress),
						maxItem = arrayMax(carsInGroups),
						actualActiveGroup = semObj.activeGroup;
					//console.log(semObj.id, carsInGroups);
					if(maxItem.id == -1) {
						semObj.disableGroup((semObj.activeGroup+1) % semObj.gLength);
					} else if(maxItem.id != semObj.activeGroup) {
						semObj.disableGroup(maxItem.id);
					}
					semObj.tmpCounter = 0;
					if (actualActiveGroup != semObj.activeGroup)
						semObj.tmpCounterMax = 0;
				}
			}
		},
        advancedSmart : {
			counter: 22,
			maxCounter: 200,
			checkAndChange: function(semObj) {
				var me = this, pointsOfInteress = 50,
                    groupToEnable = -1, score = 0, fewCarRate = 4;
				semObj.tmpCounter = semObj.tmpCounter || 0;
				semObj.tmpCounterMax = semObj.tmpCounterMax || 0;

				
				 if(semObj.tmpCounterMax++ >= me.maxCounter) {
                    semObj.disableGroup((semObj.activeGroup+1) % semObj.gLength);
                    semObj.tmpCounterMax = 0;
                    semObj.tmpCounter = 0;
                }
				else if(semObj.tmpCounter++ == me.counter) {
					var	carsInGroups = semObj.getCarsInGroups(pointsOfInteress),
						maxItem = {},
                        minItem,
						actualActiveGroup = semObj.activeGroup;
					if (carsInGroups[0] == carsInGroups[1]) 
						maxItem.id = -1;
					else
						maxItem = arrayMax(carsInGroups);
                    var numCars = 0;
                    for(var i = 0; i < semObj.adiacentSems.length; i++) {
                        if(semObj.adiacentSems[i] && semObj.adiacentSems[i].sem.activeGroup != semObj.adiacentSems[i].group) {
                            numCars = semObj.adiacentSems[i].sem.countCarsInLane(semObj.adiacentSems[i].lane);
                            score = Math.ceil(numCars/fewCarRate);
                            score = (score > 3) ? 3 : score;
                            if(score >= 2) {
                                var pLane = semObj.map.getParallelLane(semObj.adiacentSems[i].lane);
                                carsInGroups = semObj.getCarsInGroups(pointsOfInteress, pLane);
                                if(carsInGroups[0] < carsInGroups[1]) {
                                    groupToEnable = 0;
                                    break;
                                } else if(carsInGroups[1] < carsInGroups[0]) {
                                    groupToEnable = 1;
                                    break;
                                }
                            }
                        }
                    }
                    
                    
                    if(groupToEnable == -1) {
                        if(maxItem.id == -1) {
                            groupToEnable = (semObj.activeGroup+1) % semObj.gLength;
                        } else if(maxItem.id != semObj.activeGroup) {
                            groupToEnable = maxItem.id;
                        }
                    }
                    
                    if(groupToEnable != -1 && groupToEnable != semObj.activeGroup) {
                        semObj.disableGroup(groupToEnable);
                    }
                    
					semObj.tmpCounter = 0;
					if (actualActiveGroup != semObj.activeGroup)
						semObj.tmpCounterMax = 0;
				}
			}
		},
		rand : {
			checkAndChange: function(semObj) {
				var me = this;
				semObj.counter = semObj.counter || getRandomInt(22, 200);
				semObj.tmpCounter = semObj.tmpCounter || 0;
				if(semObj.tmpCounter++ == semObj.counter) {
					semObj.disableGroup((semObj.activeGroup+1) % semObj.gLength);
					semObj.counter = semObj.counter || getRandomInt(22, 200);
					semObj.tmpCounter = 0;
				}
			}
		}
	};
	
    $("#semLogic").append('<option value="all">all</option>');
	$.each(semaphores, function(index, obj) {
		$("#semLogic").append('<option value="'+index+'">'+index+'</option>');
	});
	$($('#semLogic option')[0]).attr("selected", "true");
    semaphores.selectedSem = $('#semLogic').val();
    $('#semLogic').change(function() {
        var sem = $(this).val();
        if(semaphores[semaphores.selectedSem] && $.isFunction(semaphores[semaphores.selectedSem].onDeselect)) {
            $.proxy(semaphores[semaphores.selectedSem].onDeselect, semaphores[semaphores.selectedSem])();
        }
        if(semaphores[sem] && $.isFunction(semaphores[sem].onSelect)) {
            $.proxy(semaphores[sem].onSelect, semaphores[sem])();
        }
        semaphores.selectedSem = sem;
    });
}

function initAppElements() {
	var wSize = getPageSize();
	$("#addCasualCar").click(function() {
		addCar();
	});
    $('fieldset.dna, #progressInfo').hide();
    $('#route').customSelect();
    
    $('#viewResult, #viewDNAResult').toggleClass("disabled");
    $("#chartContainer").dialog({ height: wSize.h-100, width:wSize.w-100  });
    $('#viewDNAResult').click(function() {
		if(!$('#viewDNAResult.disabled')[0]) {
			$('#chartContainer').dialog( "open" );
		var data = [];
                var drilldownSeries = [];
		
                for (var k = 0; k< finishedGeneration.results.length; k++){
                        data.push(
                                {name: "Generation "+ (k+1),
                                y: finishedGeneration.results[k].avgScore,
        			drilldown: "gen"+k}
                        );
                        var genLocalData = [];
                        for (var j = 0; j< finishedGeneration.results[k].simulations.length; j++){
					var lsim = finishedGeneration.results[k].simulations[j];   
									genLocalData.push(
											{name: "Sim "+j,
											y: lsim.result.avgScore,
						drilldown: "sim"+j}
									);
					var simLocalData = [];
					for (var h = 0; h < lsim.result.cars.length; h++){
						simLocalData.push(
							{name: "Car "+h,
							y: lsim.result.cars[h].score}
						);				
					}
					drilldownSeries.push(
						{name: "Simulation "+(j+1),
						id: "sim"+j,
						data: simLocalData}
					);
							}
							drilldownSeries.push(
									{name: "Generation "+(k+1),
									id: "gen"+k,
									data: genLocalData}
							);
					
					
                }
                createChart("#chartContainer",  [{
							name: 'Generation',
							colorByPoint: true,
							data: data
						}],{
							series: drilldownSeries
						});
		}
	});
    
    $('#viewResult').click(function() {
		if(!$('#viewResult.disabled')[0]) {
			$('#chartContainer').dialog( "open" );
            var data = [];
            var drilldownSeries = [];

            for (var k = 0; k< results.length; k++){
                    data.push({name: results[k].semaphore,
                            y: results[k].avgScore,
                            drilldown: "gen"+k});
                            
                    var simLocalData = [];
                    for (var h = 0; h < results[k].cars.length; h++){
                        simLocalData.push(
                            {name: "Car "+h,
                            y: results[k].cars[h].score}
                        );				
                    }

                    drilldownSeries.push(
                            {name: "Simulation "+(k+1),
                            id: "gen"+k,
                            data: simLocalData}
                    );
                
            }
            createChart("#chartContainer",  [{
                        name: 'Simulation',
                        colorByPoint: true,
                        data: data
                    }],{
                        series: drilldownSeries
                }); 
		}
	});
    
    $('#colorSelector').ColorPicker({
		color: '#0000ff',
		onShow: function (colpkr) {
			$(colpkr).fadeIn(500);
			return false;
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			$('#colorSelector div').css('backgroundColor', 'rgb(' + rgb.r+','+ rgb.g + ',' + rgb.b +')');
		}
	});
	
	initSemaphores();
	setRandomColor();
    setElementsSize();
    
    $(window).resize(setElementsSize);
    $('#chartContainer').dialog( "close" );
    
    $('.control.about').click(function() {
        $( "#dialog-about" ).dialog("open");
    });
    
    $( "#dialog-about" ).dialog({
        modal: true,
        height: wSize.h-100, width:wSize.w-100
    }).dialog("close");
}

function initApp(config) {
	map = new Map("data/"+config.map.url, $("#canvas")[0].getContext("2d"), $("#canvasDynamic")[0].getContext("2d"), $.proxy(applyLayoutConfig, this, config), config.stepsLimit);
    map.layoutConfig = config;
	
    $("#generateDna").click(function() {
        $("#simulationList").html("");
        $('#generateDna').toggleClass("disabled");
        $('#saveDna').toggleClass("disabled");
        $('#progressbarGen').val(0);
        $('#progressInfo').show();
        generation = new Generation("#delayInput", function(bestDna) {
            $('#viewDNAResult').removeClass("disabled");
            console.log(bestDna);
            semaphores.dna.setDna(bestDna);
            $('#generateDna').toggleClass("disabled");
            $('#progressInfo').hide();
            finishedGeneration = generation;
            generation = null;
        });
        generation.start();
    });
    
    $("#saveDna").click(function() {
        if($('#saveDna.disabled')[0]) return;
        if(generation) {
            generation.finish();
            $('fieldset.dna').hide();
        } else {
            var dna = $('#dnaData textarea').val();
            if(dna) {
                semaphores.dna.setDna(dna);
                $('fieldset.dna').hide();
            } else {
                
            }
        }
    });
    $("#startSimulation").click(function() {
        var semLogic = $("#semLogic").val();
        results = [];
        resultsAvg = [];
        map.nCarsIndex = 0;
        map.simRepeat = 0;
        if(semLogic == "all") {
            startAllSimulations();
        } else {
            startSimulation(semLogic);
        }
    });
    initializeDelaySelector({min: 10, max: 200, step:-10, value: 100});
}

function startSimulation(semLogic, onFinish) {
    $('#startSimulation').toggleClass("disabled");
    if($.isFunction(semaphores[semLogic].isErrorState)) {
        var error = semaphores[semLogic].isErrorState();
        if(error) {
            alert(error);
            $('#startSimulation').toggleClass("disabled");
            return;
        }
    }
    if($.isArray(map.layoutConfig.cars.number)) {
        applyLayoutConfig(map.layoutConfig, map.nCarsIndex);
    }
    simulation = new Simulation("#delayInput", false, function(result) {
        var repeat = false;
        results.push(result);
        $('#startSimulation').toggleClass("disabled");
        $('#viewResult.disabled').toggleClass("disabled");
        $("#simulationList").append('<li>'+ result.semaphore+ '('+result.cars.length+') - ' +result.avgScore+' - '+result.avgSteps+'</li>');
        $("#simulationList").scrollTop($("#simulationList")[0].scrollHeight);
        map.simRepeat++;
        if(map.layoutConfig.simulationRepeat && map.simRepeat < parseInt(map.layoutConfig.simulationRepeat)) {
            startSimulation(semLogic, onFinish);
            repeat = true;
        } else if($.isArray(map.layoutConfig.cars.number) && map.nCarsIndex+1 < map.layoutConfig.cars.number.length) {
            repeat = true;
            saveAvgs(result);
            map.simRepeat = 0;
            map.nCarsIndex++;
            applyLayoutConfig(map.layoutConfig, map.nCarsIndex);
            startSimulation(semLogic, onFinish);
        } else if(parseInt(map.layoutConfig.simulationRepeat) > 1) {
            saveAvgs(result);
        }
        
        if(!repeat && $.isFunction(onFinish)) {
            onFinish(result);
        }
    });
    $("#semLogic").val(semLogic);
    simulation.start(false, semLogic);
}


function saveAvgs(result) {
    var semsResult = results.filter(function(res) {
        return (res.semaphore == result.semaphore && res.cars.length == result.cars.length);
    });
    var avgAvgScore = arrayAverage(semsResult.map(function(res) {
        return res.avgScore;
    }));
    var avgAvgSteps = arrayAverage(semsResult.map(function(res) {
        return res.avgSteps;
    }));
    
    avgAvgScore = parseFloat(avgAvgScore.toFixed(4));
    avgAvgSteps = Math.round(avgAvgSteps);
    
    resultsAvg.push({
        sem: result.semaphore,
        cars: result.cars.length,
        score: avgAvgScore,
        steps: avgAvgSteps
    });
    $("#simulationList").append('<li><b>'+ result.semaphore+ '('+result.cars.length+') - ' +avgAvgScore+' - '+avgAvgSteps+'</b></li>');
    $("#simulationList").scrollTop($("#simulationList")[0].scrollHeight);
}

function startAllSimulations() {
    $("#simulationList").html("");
    results = [];
    var semLogics = $("#semLogic option").filter(function(index, el) {
        return $(el).val() != "all";
    }).map(function(index, el) { return $(el).val()});
    startSequenceSimulations(semLogics);
}

function startSequenceSimulations(sequence) {
    if(sequence.length) {
        var sem = sequence[0];
        map.nCarsIndex = 0;
        map.simRepeat = 0;
        startSimulation(sem, function() {
            if(resultsAvg.length) {
                var semsResult = resultsAvg.filter(function(res) {
                    return (res.sem == sem);
                });
                var avgAvgScore = arrayAverage(semsResult.map(function(res) {
                    return res.score;
                }));
                var avgAvgSteps = arrayAverage(semsResult.map(function(res) {
                    return res.steps;
                }));
                
                avgAvgScore = parseFloat(avgAvgScore.toFixed(4));
                avgAvgSteps = Math.round(avgAvgSteps);
                $("#simulationList").append('<li><b>'+ sem+ ' - ' +avgAvgScore+' - '+avgAvgSteps+'</b></li>');
                $("#simulationList").scrollTop($("#simulationList")[0].scrollHeight);
            }
            startSequenceSimulations(sequence.slice(1));
        });
        
    } else {
        $('#viewResult.disabled').toggleClass("disabled");
    }
}

function buildRouteList(routeList) {
	var res = "";
	
	$.each(routeList, function(index, obj) {
		var percent = (obj.carsPercent) ? obj.carsPercent+"%" : "Random";
		res+="<li>"+obj.route +" - "+percent+"</li>";
	});
			
	return res;
}

function showSimulationLayouts(layouts) {
	var layoutContainer = $("#layoutSelection");
	layoutContainer.show();
	$.each(layouts, function(index, layout){
		var itemHtml = "", cars = (layout.cars.number) ? layout.cars.number : false,
			icon = layout.map.icon || "images/map.png", 
			carsDistribution = (layout.cars.dRoutes) ? buildRouteList(layout.cars.dRoutes) : false;
		cars = (!cars && layout.cars.rangeNumber) ? 
				"Random ("+layout.cars.rangeNumber.min+":"+layout.cars.rangeNumber.max+")" : cars;
		
		itemHtml+= '<span class="descItem"><span class="label">Map</span>'+layout.map.name+'</span>';
		itemHtml+= '<span class="descItem"><span class="label">Cars number</span>'+cars+'</span>';
		itemHtml+= (carsDistribution) ? '<div class="descItem"><span class="label">Cars distribution</span><ul>'+carsDistribution+'</ul></div>' 
										 : '<span class="descItem"><span class="label">Cars distribution</span>Random</span>';
		layoutContainer.append('<div layoutindex="'+index+'"><img src="'+icon+'"><div>'+itemHtml+'</div><div class="clear"></div></div>');
	});
	$("div[layoutindex]").click(function() {
		var layout = layouts[$(this).attr("layoutindex")];
		if(layout) {
			initApp(layout);
			$(".topLayer").hide(300);
		}
	});
}

var map = null, simulation = null, semaphores = null, generation = null, results = [], resultsAvg = [];

require.config({baseUrl: 'js/lib', paths: {
	Highcharts: 'highcharts',
	jQuery: 'jquery'
}});
require(['jquery'], function ($) {
	require(['jquery-ui', 'colorpicker', 'jquery.customSelect.min', 'Highcharts'], function() {
			require(['modules/drilldown'], function(){
				require.config({baseUrl: 'js'});
				require(["utils", "Car", "Semaphore", "Map", "Simulation", "Generation", "Chart"], function() {
					$(document).ready(function () {
						initAppElements();
						$.getJSON("data/simulationLayouts.json", showSimulationLayouts);
					});
				});
			});
		}
	);
});
