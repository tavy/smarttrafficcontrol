function Semaphore(id, config, map, ctx) {
	this.id = id;
	this.map = map;
	this.groups = [];
	this.lanes = [];
	this.coordinates = [];
	this.ctx = ctx;
    this.activeGroup = null;
	this.colors = ["rgb(255,0,0)","rgb(0,255,0)","rgb(255,255,0)"]
	
	this.draw = function() {
		var me = this;
		$.each(me.groups, function(index, group) {
			$.each(group.lanes, function(index, lane) {
				me.drawLight(lane, me.colors[group.state]);
			});
		});
	}
	
	this.start = function() {
		var me = this, randGroup;
		me.gLength = me.groups.length;
        me.tmpCounter = 0;
		randGroup = getRandomInt(0,me.gLength-1);
        randTimeOut = getRandomInt(0, me.map.delay*50);
        $.each(me.groups, function(index, group) {
			me.changeState(index, 0);
		});
        //setTimeout(function() {
            me.enableGroup(randGroup);
        //}, randTimeOut);
	}
	
	this.enableGroup = function(idG) {
		var me = this;
		me.activeGroup = idG;
		me.changeState(idG, 1);
	}
	
	this.disableGroup = function(idG) {
		var me = this;
		me.changeState(me.activeGroup, 2);
		setTimeout(function() {
			me.changeState(me.activeGroup, 0);
			me.enableGroup(idG);
		}, me.map.delay*6);
	}
	
	this.changeState = function(idG, state) {
		var me = this;
        if(me.groups[idG]) {
            me.groups[idG].state = state;
            $.each(me.groups[idG].lanes, function(index, lane) {
                me.drawLight(lane, me.colors[state]);
            });
        }
	}
	
	this.drawLight = function(lane, color) {
		var ctx = this.ctx, lCoordinates = lane.coordinates.length,
			co1 = lane.coordinates[lCoordinates-1],
			co2 = lane.coordinates[lCoordinates-2];
			
			ctx.beginPath();
			ctx.strokeStyle = color;
			ctx.lineWidth = this.map.edgeWidth;
			ctx.moveTo(co1.x, co1.y);
			ctx.lineTo(co2.x, co2.y);
			ctx.stroke();
	}
	
	this.getLaneByBbox = function(bbox) {
		for(var i = 0, lLane = this.coordinates.length; i < lLane; i++) {
			if(bboxOverlap(bbox, this.coordinates[i])) {
				return this.lanes[i];
			}
		}
		return null;
	}
	
	this.canPassLane = function(lane) {
		for(var i = 0, lGroups = this.groups.length; i < lGroups; i++) {
			if($.inArray(lane, this.groups[i].lanes) != -1) {
				return this.groups[i].state == 1;
			}
		}
	}
	
	this.getCarsInGroups = function(points, laneToExclude) {
		var counters = [];
		for(var i = 0, lCoord = this.groups.length; i < lCoord; i++) {
			counters.push(this.countCarsInGroup(this.groups[i], points, laneToExclude));
		}
		return counters;
	}
	
	this.countCarsInGroup = function(group, points, laneToExclude) {
		var counter = 0;
		for(var i = 0, lCoord = group.lanes.length; i < lCoord; i++) {
            if(!laneToExclude || laneToExclude != group.lanes[i]) {
                counter += this.countCarsInLane(group.lanes[i], points);
            }
		}
		return counter;
	}
	
	this.countCarsInLane = function(lane, points) {
		var counter = 0, lCoords = lane.coordinates.length;
		points = points || lCoords;
        points = Math.min(points, lCoords);
		for(limit = lCoords-points, i = lCoords-1; i > limit; i--) {
			if(lane.coordinates[i].taken === true) {
				counter++;
			}
		}
		return counter;
	}
    
    this.createSemNetwork = function() {
        var me = this;
        me.adiacentSems = [];
        $.each(me.groups, function(index, group) {
			$.each(group.lanes, function(index, lane) {
                var pLane = me.map.getParallelLane(lane);
				var sem = me.map.getSemaphoreByLane(pLane);
                me.adiacentSems.push(sem);
			});
		});
    }
	
	this.init = function(config) {
		var me = this;
		$.each(config.groups, function(index, lanes) {
			var ln = [];
			$.each(lanes, function(index, id) {
				var lane = me.map.getLaneById(id),
					co;
				me.map.computeRouteLane(lane);
				ln.push(lane);
				co = lane.coordinates[lane.coordinates.length-2];
				me.coordinates.push({x:co.x, y:co.y, w:1, h:me.map.edgeWidth});
				me.lanes.push(lane);
			});
            me.junction = map.getJunctionIncludingLanes(me.lanes);
			me.groups.push({lanes:ln, state: 0});
        });
	}
	
	this.existLane = function(lane) {
		for(var i = 0, lLane = this.lanes.length; i < lLane; i++) {
			if(lane == this.lanes[i]) {
				return true;
			}
		}
		return false;
	}
	
	this.init(config);
}
